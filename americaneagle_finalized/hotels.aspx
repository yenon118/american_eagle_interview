<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Hotels.aspx.cs" Inherits="Hotels" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Hotels</title>
</head>
<body>

    <p style="color:red;"><b>Please put your work on these placeholder pages and use code-behind class files so we can see your code (please don't compile into DLL files):</b><br/></p>

    <a href="/">&laquo; Back To Home</a>

    <p>
        Please display the Hotel Name, Airport Code, and Address for the hotel with 
        the ID of 105304 using this web service:
        <br/>
        http://ws-design.idevdesign.net/hotels.asmx?op=GetHotel
    </p>

    <p>
        This link gives more details related to this web service:
        <br/>
        http://ws-design.idevdesign.net/docs/hotels.html
    </p>

    <p>You will need the following credentials to access the web service:</p>

    <p>Login: aeTraining</p>

    <p>Password: ZZZ</p>

    <form id="main" runat="server">
        <div>
            <asp:GridView ID="GridView6" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="hotelName" HeaderText="Name" />
                    <asp:BoundField DataField="hotelAirportCode" HeaderText="Airport Code" />
                    <asp:BoundField DataField="hotelAddress" HeaderText="Address" />
                    <%--<asp:BoundField DataField="Address2" HeaderText="Address2" />--%>
                    <%--<asp:BoundField DataField="Address3" HeaderText="Address3" />--%>
                </Columns>
            </asp:GridView>
        </div>
    </form>


</body>
</html>
