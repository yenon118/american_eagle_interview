using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Validator : System.Web.UI.Page
{
    // protected global::System.Web.UI.HtmlControls.HtmlForm form1;
    // protected global::System.Web.UI.WebControls.DropDownList drpState;
    // protected global::System.Web.UI.WebControls.TextBox txtRegion;
    // protected global::System.Web.UI.WebControls.Button btnSubmit;
    // protected global::System.Web.UI.WebControls.CustomValidator CustomValidator;



    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void serverSideValidation(object source, ServerValidateEventArgs args)
    {
        if (txtRegion.Text.Length == 0 && drpState.SelectedItem.Text.Equals(""))
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }
}
