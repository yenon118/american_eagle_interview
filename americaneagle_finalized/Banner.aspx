<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Banner.aspx.cs" Inherits="Banner" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Web App</title>
</head>
<body>

    <p style="color:red;"><b>Please put your work on these placeholder pages and use code-behind class files so we can see your code (please don't compile into DLL files):</b><br/></p>

    <a href="/">&laquo; Back To Home</a>

    <form id="main" runat="server">
        <div>
            <p>There are two2 tables in the database "webapp" called: <b>Banner</b> and <b>BannerTracking</b><br/></p>
       
            <p>
                Please create query that returns:<br/>
                Banner Name and Banner Link columns and sum of all all impressions and sum of all clicks since 11/21/2006
            </p>
            <asp:GridView ID="GridView5" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="name" HeaderText="Name" />
                    <asp:BoundField DataField="link" HeaderText="Link" />
                    <asp:BoundField DataField="impressionCount" HeaderText="ImpressionCount" />
                    <asp:BoundField DataField="clickCount" HeaderText="ClickCount" />
                </Columns>
            </asp:GridView>

            <p>Please bind the result to the gridview and display this gridview below</p>

            <p><b>Banner</b> table<br/><img src="table1.gif" alt=""/></p>
            <asp:GridView ID="GridView1" runat="server" DataKeyNames="ordinalPosition" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="columnName" HeaderText="Column Name" />
                    <asp:BoundField DataField="typeName" HeaderText="Data Type" />
                    <asp:BoundField DataField="length" HeaderText="Length" />
                    <asp:BoundField DataField="nullable" HeaderText="Allow Nulls" />
                </Columns>
            </asp:GridView>
            <p><b>BannerTracking</b> table<br/><img src="table2.gif" alt=""/></p>
            <asp:GridView ID="GridView2" runat="server" DataKeyNames="ordinalPosition" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="columnName" HeaderText="Column Name" />
                    <asp:BoundField DataField="typeName" HeaderText="Data Type" />
                    <asp:BoundField DataField="length" HeaderText="Length" />
                    <asp:BoundField DataField="nullable" HeaderText="Allow Nulls" />
                </Columns>
            </asp:GridView>
            <p><b>Banner</b> table data<br/><img src="Banner_data.gif" alt=""/></p>
            <asp:GridView ID="GridView3" runat="server" DataKeyNames="bannerId" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="bannerId" HeaderText="BannerId" />
                    <asp:BoundField DataField="name" HeaderText="Name" />
                    <asp:BoundField DataField="link" HeaderText="Link" />
                    <asp:BoundField DataField="image" HeaderText="Image" />
                    <asp:BoundField DataField="altText" HeaderText="Alt Text" />
                    <asp:BoundField DataField="targetString" HeaderText="Target" />
                    <asp:BoundField DataField="isActive" HeaderText="isActive" />
                </Columns>
            </asp:GridView>
            <p><b>BannerTracking</b> table data<br/><img src="BannerTracking_data.gif" alt=""/></p>
            <asp:GridView ID="GridView4" runat="server" DataKeyNames="trackingId" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="trackingId" HeaderText="TrackingId" />
                    <asp:BoundField DataField="bannerId" HeaderText="BannerId" />
                    <asp:BoundField DataField="impressionCount" HeaderText="ImpressionCount" />
                    <asp:BoundField DataField="clickCount" HeaderText="ClickCount" />
                    <asp:BoundField DataField="createDate" HeaderText="CreateDate" />
                </Columns>
            </asp:GridView>
        </div>
    </form>

</body>
</html>
