using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

public class HotelViewModel
{
    public int hotelId { get; set; }
    public string hotelName { get; set; }
    public string hotelAirportCode { get; set; }
    public string hotelAddress { get; set; }
    public string hotelAddress1 { get; set; }
    public string hotelAddress2 { get; set; }
    public string hotelAddress3 { get; set; }
    public string hotelCity { get; set; }
    public string hotelState { get; set; }
    public string hotelCountry { get; set; }
    public string hotelPostalCode { get; set; }

}


public static class Soap
{
    private static HttpWebRequest createRequest()
    {
        HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://ws-design.idevdesign.net/hotels.asmx");
        httpWebRequest.Method = "POST";
        httpWebRequest.Host = "ws-design.idevdesign.net";
        httpWebRequest.Headers.Add(@"SOAPAction:http://ws.design.americaneagle.com/GetHotel");
        httpWebRequest.ContentType = "text/xml; charset=utf-8";
        httpWebRequest.Accept = "text/xml";

        return httpWebRequest;
    }

    private static string postRequest()
    {
        String username = "aeTraining";
        String password = "ZZZ";
        int hotelId = 105304;

        Stream stream = null;
        WebResponse webResponse = null;
        StreamReader streamReader = null;

        string result = "";

        HttpWebRequest httpWebRequest = createRequest();
        XmlDocument xmlDocument = new XmlDocument();
        string xmlString = @"<?xml version=""1.0"" encoding=""utf-8""?>" +
                            @"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                                @"<soap:Header>" +
                                    @"<HotelCredentials xmlns=""http://ws.design.americaneagle.com"">" +
                                        @"<username>" + username + @"</username>" +
                                        @"<password>" + password + @"</password>" +
                                    @"</HotelCredentials>" +
                                //@"<ImageOptions xmlns=""http://ws.design.americaneagle.com"">" +
                                //    @"<includeImages>" + boolean + @"</includeImages>" +
                                //    @"<maxWidth>" + int + @"</maxWidth>" +
                                //    @"<maxHeight>" + int + @"</maxHeight>" +
                                //    @"<maxThumbnailWidth>" + int + @"</maxThumbnailWidth>" +
                                //    @"<maxThumbnailHeight>" + int + @"</maxThumbnailHeight>" +
                                //@"</ImageOptions>" +
                                @"</soap:Header>" +
                                @"<soap:Body>" +
                                    @"<GetHotel xmlns=""http://ws.design.americaneagle.com"">" +
                                        @"<hotelId>" + hotelId + @"</hotelId>" +
                                    @"</GetHotel>" +
                                @"</soap:Body>" +
                            @"</soap:Envelope>";
        //Console.WriteLine(xmlString);
        xmlDocument.LoadXml(xmlString);
        try
        {
            stream = httpWebRequest.GetRequestStream();
            xmlDocument.Save(stream);

            webResponse = httpWebRequest.GetResponse();
            streamReader = new StreamReader(webResponse.GetResponseStream());
            result = streamReader.ReadToEnd();
        }
        catch (Exception exception)
        {
            Console.WriteLine("Error: \n{0}\n\n\n", exception);
        }
        finally
        {
            stream.Dispose();
            webResponse.Dispose();
            streamReader.Dispose();
        }
        
        return result;
    }

    public static List<HotelViewModel> getHotels()
    {
        List<HotelViewModel> hotels = new List<HotelViewModel>();

        string xmlResult = postRequest();

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(xmlResult);

        XmlNamespaceManager xmlNamespaceManager = new XmlNamespaceManager(xmlDocument.NameTable);
        xmlNamespaceManager.AddNamespace("soap", "http://schemas.xmlsoap.org/soap/envelope/");
        xmlNamespaceManager.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        xmlNamespaceManager.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
        xmlNamespaceManager.AddNamespace("xhr", "http://ws.design.americaneagle.com");

        XmlNodeList xmlNodeList = xmlDocument.SelectNodes("//xhr:GetHotelResult", xmlNamespaceManager);

        foreach (XmlNode xmlNode in xmlNodeList)
        {
            HotelViewModel tempHotel = new HotelViewModel();

            try
            {
                tempHotel.hotelId = Convert.ToInt32(xmlNode["HotelID"].InnerText.ToString());
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel ID \n{0}\n", exception);
                tempHotel.hotelId = 0;
            }
            try
            {
                tempHotel.hotelName = xmlNode["Name"].InnerText.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel Name \n{0}\n", exception);
                tempHotel.hotelName = "";
            }
            try
            {
                tempHotel.hotelAirportCode = xmlNode["AirportCode"].InnerText.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel Airport Code \n{0}\n", exception);
                tempHotel.hotelAirportCode = "";
            }
            try
            {
                tempHotel.hotelAddress1 = xmlNode["Address1"].InnerText.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel Address 1 \n{0}\n", exception);
                tempHotel.hotelAddress1 = "";
            }
            try
            {
                tempHotel.hotelAddress2 = xmlNode["Address2"].InnerText.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel Address 2 \n{0}\n", exception);
                tempHotel.hotelAddress2 = "";
            }
            try
            {
                tempHotel.hotelAddress3 = xmlNode["Address3"].InnerText.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel Address 3 \n{0}\n", exception);
                tempHotel.hotelAddress3 = "";
            }
            try
            {
                tempHotel.hotelCity = xmlNode["City"].InnerText.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel City \n{0}\n", exception);
                tempHotel.hotelCity = "";
            }
            try
            {
                tempHotel.hotelState = xmlNode["StateProvince"].InnerText.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel State \n{0}\n", exception);
                tempHotel.hotelState = "";
            }
            try
            {
                tempHotel.hotelCountry = xmlNode["Country"].InnerText.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel Country \n{0}\n", exception);
                tempHotel.hotelCountry = "";
            }
            try
            {
                tempHotel.hotelPostalCode = xmlNode["PostalCode"].InnerText.ToString();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: Hotel Postal Code \n{0}\n", exception);
                tempHotel.hotelPostalCode = "";
            }

            //tempHotel.hotelId = Convert.ToInt32(xmlNode["HotelID"].InnerText.ToString());
            //tempHotel.hotelName = xmlNode["Name"].InnerText.ToString();
            //tempHotel.hotelAirportCode = xmlNode["AirportCode"].InnerText.ToString();
            //tempHotel.hotelAddress1 = xmlNode["Address1"].InnerText.ToString();
            //tempHotel.hotelAddress2 = xmlNode["Address2"].InnerText.ToString();
            //tempHotel.hotelAddress3 = xmlNode["Address3"].InnerText.ToString();
            //tempHotel.hotelCity = xmlNode["City"].InnerText.ToString();
            //tempHotel.hotelState = xmlNode["StateProvince"].InnerText.ToString();
            //tempHotel.hotelCountry = xmlNode["Country"].InnerText.ToString();
            //tempHotel.hotelPostalCode = xmlNode["PostalCode"].InnerText.ToString();

            tempHotel.hotelAddress = tempHotel.hotelAddress1 + " " + tempHotel.hotelAddress2 + " " + tempHotel.hotelAddress3 + ", \n" +
                                        tempHotel.hotelCity + ", " + 
                                        tempHotel.hotelPostalCode + "  " + tempHotel.hotelState + "\n" +
                                        tempHotel.hotelCountry;

            //Console.WriteLine(tempHotel.hotelId + "\t\t" +
            //                    tempHotel.hotelName + "\t\t" + 
            //                    tempHotel.hotelAirportCode + "\t\t" + 
            //                    tempHotel.hotelAddress1 + "\t\t" +
            //                    tempHotel.hotelAddress2 + "\t\t" +
            //                    tempHotel.hotelAddress3 + "\t\t" +
            //                    tempHotel.hotelCity + "\t\t" +
            //                    tempHotel.hotelState + "\t\t" +
            //                    tempHotel.hotelPostalCode + "\t\t" +
            //                    tempHotel.hotelCountry);

            //Console.WriteLine(tempHotel.hotelAddress);

            hotels.Add(tempHotel);
        }
        
        return hotels;
    }
}



public partial class Hotels : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            List<HotelViewModel> hotelList = Soap.getHotels();

            try{
                GridView6.DataSource = hotelList;
                GridView6.DataBind();
            }catch (Exception exception){
                Console.WriteLine("Error: \n{0}\n\n\n", exception);
            }
        }
        

    }
}
