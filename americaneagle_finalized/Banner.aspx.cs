using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;



public class BannerBannerViewModel
{
    //public String tableQualifier { get; set; }
    //public String tableOwner { get; set; }
    //public String tableName { get; set; }

    public String columnName { get; set; }

    //public int dataType { get; set; }

    public String typeName { get; set; }

    //public int precision { get; set; }

    public int length { get; set; }

    //public int scale { get; set; }
    //public int radix { get; set; }

    public int nullable { get; set; }

    //public int remarks { get; set; }
    //public int columnDef { get; set; }
    //public int sqlDataType { get; set; }
    //public int sqlDatetimeSub { get; set; }
    //public int charOctetLength { get; set; }

    public int ordinalPosition { get; set; }

    //public String isNullable { get; set; }
    //public int ssDataType { get; set; }

    //public bool boolIsNullable
    //{
    //    get
    //    {
    //        if (isNullable.Equals("YES"))
    //        {
    //            return true;
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }
    //}
}



public class BannerDataViewModel
{
    public int bannerId { get; set; }

    public string name { get; set; }
    public string link { get; set; }
    public string image { get; set; }
    public string altText { get; set; }
    private string target;
    public string targetString
    {
        get
        {
            return target;
        }
        set
        {
            if (value.Equals("") || value == null)
            {
                target = "NULL";
            }
        }
    }
    public int isActive { get; set; }

    public int trackingId { get; set; }
    public int impressionCount { get; set; }
    public int clickCount { get; set; }
    public string createDate { get; set; }

}

public class BannerDAL
{
    private string connectionString;

    public BannerDAL(string connectionString)
    {
        this.connectionString = connectionString;
    }

    public List<BannerBannerViewModel> getBannerColumnDetails()
    {
        string sqlQuery = "exec sp_columns Banner;";
        List<BannerBannerViewModel> bannerColumnsInfo = new List<BannerBannerViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerBannerViewModel temp = new BannerBannerViewModel()
                    {
                        columnName = reader["COLUMN_NAME"].ToString(),
                        typeName = reader["TYPE_NAME"].ToString(),
                        length = Convert.ToInt32(reader["LENGTH"]),
                        nullable = Convert.ToInt32(reader["NULLABLE"]),
                        ordinalPosition = Convert.ToInt32(reader["ORDINAL_POSITION"])
                    };
                    bannerColumnsInfo.Add(temp);
                }
            }
        }
        return bannerColumnsInfo;
    }

    public List<BannerBannerViewModel> getBannerTrackingColumnDetails()
    {
        string sqlQuery = "exec sp_columns BannerTracking;";
        List<BannerBannerViewModel> bannerTrackingColumnsInfo = new List<BannerBannerViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerBannerViewModel temp = new BannerBannerViewModel()
                    {
                        columnName = reader["COLUMN_NAME"].ToString(),
                        typeName = reader["TYPE_NAME"].ToString(),
                        length = Convert.ToInt32(reader["LENGTH"]),
                        nullable = Convert.ToInt32(reader["NULLABLE"]),
                        ordinalPosition = Convert.ToInt32(reader["ORDINAL_POSITION"])
                    };
                    bannerTrackingColumnsInfo.Add(temp);
                }
            }
        }
        return bannerTrackingColumnsInfo;
    }

    public List<BannerDataViewModel> getAllBannerResults()
    {
        string sqlQuery = "SELECT * ,1 AS FLG FROM Banner WHERE BannerId = 8 UNION SELECT * ,2 AS FLG FROM Banner WHERE BannerId != 8 ORDER BY FLG;";
        List<BannerDataViewModel> allBannerResults = new List<BannerDataViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerDataViewModel temp = new BannerDataViewModel()
                    {
                        bannerId = Convert.ToInt32(reader["BannerId"]),
                        name = reader["Name"].ToString(),
                        link = reader["Link"].ToString(),
                        image = reader["Image"].ToString(),
                        altText = reader.GetString(4).ToString(),
                        targetString = (reader["Target"]).ToString(),
                        isActive = Convert.ToInt32(reader["IsActive"])
                    };
                    allBannerResults.Add(temp);
                }
            }
        }
        return allBannerResults;
    }

    public List<BannerDataViewModel> getAllBannerTrackingResults()
    {
        string sqlQuery = "SELECT * FROM BannerTracking ORDER BY BannerId;";
        List<BannerDataViewModel> allBannerTrackingResults = new List<BannerDataViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerDataViewModel temp = new BannerDataViewModel()
                    {
                        trackingId = Convert.ToInt32(reader["TrackingId"]),
                        bannerId = Convert.ToInt32(reader["BannerId"]),
                        impressionCount = Convert.ToInt32(reader["ImpressionCount"]),
                        clickCount = Convert.ToInt32(reader["ClickCount"]),
                        createDate = reader["CreateDate"].ToString()
                    };
                    allBannerTrackingResults.Add(temp);
                }
            }
        }
        return allBannerTrackingResults;
    }

    public List<BannerDataViewModel> getCustomizedResults()
    {
        string sqlQuery = "SELECT b.Name, b.Link, SUM(t.ImpressionCount) AS SumImpressionCount, SUM(t.ClickCount) AS SumClickCount " + 
                            "FROM Banner b JOIN BannerTracking AS t ON b.BannerId = t.BannerId " + 
                            "WHERE t.CreateDate >= Convert(datetime, '2006-11-21') " + 
                            "GROUP BY b.Name, b.Link; ";
        List<BannerDataViewModel> customizedResults = new List<BannerDataViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerDataViewModel temp = new BannerDataViewModel()
                    {
                        name = reader["Name"].ToString(),
                        link = reader["Link"].ToString(),
                        impressionCount = Convert.ToInt32(reader["SumImpressionCount"]),
                        clickCount = Convert.ToInt32(reader["SumClickCount"])
                    };
                    customizedResults.Add(temp);
                }
            }
        }
        return customizedResults;
    }

}


public class BannerBLL
{
    private BannerDAL bannerDataAccessLayer;

    public BannerBLL(string connectionString)
    {
        bannerDataAccessLayer = new BannerDAL(connectionString);
    }

    public List<BannerBannerViewModel> getBannerColumnDetailsList()
    {
        List<BannerBannerViewModel> bannerColumnsInfoList = new List<BannerBannerViewModel>();
        foreach (BannerBannerViewModel detail in bannerDataAccessLayer.getBannerColumnDetails())
        {
            bannerColumnsInfoList.Add(detail);
        }
        return bannerColumnsInfoList;
    }

    public List<BannerBannerViewModel> getBannerTrackingColumnDetailsList()
    {
        List<BannerBannerViewModel> bannerTrackingColumnsInfoList = new List<BannerBannerViewModel>();
        foreach (BannerBannerViewModel detail in bannerDataAccessLayer.getBannerTrackingColumnDetails())
        {
            bannerTrackingColumnsInfoList.Add(detail);
        }
        return bannerTrackingColumnsInfoList;
    }

    public List<BannerDataViewModel> getAllBannerResultsList()
    {
        List<BannerDataViewModel> allBannerResultsList = new List<BannerDataViewModel>();
        foreach (BannerDataViewModel detail in bannerDataAccessLayer.getAllBannerResults())
        {
            allBannerResultsList.Add(detail);
        }
        return allBannerResultsList;
    }

    public List<BannerDataViewModel> getAllBannerTrackingResultsList()
    {
        List<BannerDataViewModel> allBannerTrackingResultsList = new List<BannerDataViewModel>();
        foreach (BannerDataViewModel detail in bannerDataAccessLayer.getAllBannerTrackingResults())
        {
            allBannerTrackingResultsList.Add(detail);
        }
        return allBannerTrackingResultsList;
    }

    public List<BannerDataViewModel> getCustomizedResultsList()
    {
        List<BannerDataViewModel> customizedResultsList = new List<BannerDataViewModel>();
        foreach (BannerDataViewModel detail in bannerDataAccessLayer.getCustomizedResults())
        {
            customizedResultsList.Add(detail);
        }
        return customizedResultsList;
    }

}




public partial class Banner : System.Web.UI.Page
{
    /*** Use either one (depends on which machine you run this code) ***/
    /*** You might need to edit the config file too. ***/

    private string connectionString = ConfigurationManager.AppSettings["ConnectionString"].ToString();
    // private string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindData();
        }
    }

    private void bindData()
    {
        BannerBLL bannerBLL = new BannerBLL(connectionString);

        GridView1.DataSource = bannerBLL.getBannerColumnDetailsList();
        GridView1.DataBind();

        GridView2.DataSource = bannerBLL.getBannerTrackingColumnDetailsList();
        GridView2.DataBind();

        GridView3.DataSource = bannerBLL.getAllBannerResultsList();
        GridView3.DataBind();

        GridView4.DataSource = bannerBLL.getAllBannerTrackingResultsList();
        GridView4.DataBind();

        GridView5.DataSource = bannerBLL.getCustomizedResultsList();
        GridView5.DataBind();
    }





}
