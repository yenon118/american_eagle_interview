CREATE DATABASE webapp;

use [webapp];

if not exists (select * from master.dbo.syslogins where loginname = N'webapp-iis-617') 
BEGIN 
declare @logindb nvarchar(132), @loginlang nvarchar(132) select @logindb = N'webapp', @loginlang = N'us_english' 
if @logindb is null or not exists (select * from master.dbo.sysdatabases where name = @logindb) 
select @logindb = N'master' 
if @loginlang is null or (not exists (select * from master.dbo.syslanguages where name = @loginlang) and @loginlang <>  N'us_english')select @loginlang = @@language 
exec sp_addlogin N'webapp-iis-617', 'Wp12739#', @logindb, @loginlang 
END 
GO 

if not exists (select * from dbo.sysusers where name = N'webapp-iis-617') EXEC sp_grantdbaccess N'webapp-iis-617', N'webapp-iis-617' 
GO

exec sp_addrolemember N'db_datareader', N'webapp-iis-617' 
GO

exec sp_addrolemember N'db_datawriter', N'webapp-iis-617' 
GO

/****** Object: Table [dbo].[Banner] Script Date: 5/27/2008 2:39:47 PM ******/ 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Banner]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[Banner] 
GO

/****** Object: Table [dbo].[BannerTracking] Script Date: 5/27/2008 2:39:47 PM ******/ 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BannerTracking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[BannerTracking] 
GO

/****** Object: Table [dbo].[Banner] Script Date: 5/27/2008 2:39:47 PM ******/ 
CREATE TABLE [dbo].[Banner] ( 
    [BannerId] [int] NOT NULL , 
    [Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL , 
    [Link] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL , 
    [Image] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL , 
    [AltText] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , 
    [Target] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , 
    [IsActive] [bit] NOT NULL 
) ON [PRIMARY] 
GO

/****** Object: Table [dbo].[BannerTracking] Script Date: 5/27/2008 2:39:47 PM ******/ 
CREATE TABLE [dbo].[BannerTracking] ( 
    [TrackingId] [int] NOT NULL , 
    [BannerId] [int] NOT NULL , 
    [ImpressionCount] [bigint] NULL , 
    [ClickCount] [bigint] NULL , 
    [CreateDate] [datetime] NULL 
) ON [PRIMARY] 
GO

INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (2,'Satisfaction - Large','http://localhost','logo-satisfaction-lg.gif','100% Home Satisfaction Guaranteed',NULL,'True');
INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (3,'Privacy - Large','http://localhost','logo-privacy-lg.gif','We Respect Your Privacy',NULL,'True');
INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (4,'Verisign Seal - Large','http://localhost','vsign-lg.gif','Verisign Secured - click here to verify',NULL,'True');
INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (5,'Logo - Left','http://localhost','ssf-logo.gif','A member of the School Specialty Family',NULL,'True');
INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (6,'Phone number - Left','http://localhost','nav-phone.gif','800-417-3261',NULL,'True');
INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (7,'Free Shipping - Left','http://localhost','free-shipping-177x1691.gif','Free shipping on orders over $30',NULL,'True');
INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (8,'Satisfaction -Small','http://localhost','logo-satisfaction-sm.gif','100% Home Satisfaction Guaranteed',NULL,'True');
INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (9,'Privacy -Small','http://localhost','logo-privacy-sm.gif','We Respect Your Privacy',NULL,'True');
INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (10,'Verisign Seal -Small','http://localhost','vsign-sm.gif','Verisign Secured - click here to verify',NULL,'True');
INSERT INTO [dbo].[Banner] (BannerId, Name, Link, Image, AltText, Target, IsActive) VALUES (11,'Gift Certificate','http://localhost','gift-certs-252x1852.gif','Gift Certificate',NULL,'True');

INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (3,11,101,2,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (4,6,226,2,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (5,5,211,1,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (6,2,74,0,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (7,3,72,0,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (8,4,70,1,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (9,8,117,0,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (10,9,115,0,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (11,10,113,1,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (12,7,66,1,'11/16/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (13,6,112,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (14,7,18,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (15,11,18,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (16,5,112,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (17,2,18,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (18,3,18,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (19,4,18,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (20,8,94,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (21,9,94,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (22,10,94,0,'11/17/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (23,6,1,0,'11/18/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (24,7,1,0,'11/18/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (25,11,1,0,'11/18/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (26,5,1,0,'11/18/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (27,2,1,0,'11/18/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (28,3,1,0,'11/18/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (29,4,1,0,'11/18/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (30,6,112,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (31,7,38,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (32,11,38,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (33,5,112,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (34,2,38,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (35,3,38,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (36,4,38,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (37,8,74,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (38,9,74,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (39,10,74,0,'11/20/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (40,6,235,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (41,5,235,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (42,8,103,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (43,9,103,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (44,10,103,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (45,7,132,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (46,11,132,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (47,2,132,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (48,3,132,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (49,4,132,0,'11/21/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (50,6,80,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (51,7,32,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (52,11,32,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (53,5,80,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (54,2,32,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (55,3,32,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (56,4,32,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (57,8,48,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (58,9,48,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (59,10,48,0,'11/22/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (60,6,18,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (61,7,6,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (62,11,6,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (63,5,18,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (64,2,6,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (65,3,6,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (66,4,6,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (67,8,12,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (68,9,12,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (69,10,12,0,'11/23/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (70,6,5,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (71,7,2,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (72,11,2,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (73,5,5,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (74,2,2,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (75,3,2,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (76,4,2,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (77,8,3,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (78,9,3,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (79,10,3,0,'11/25/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (80,6,187,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (81,7,50,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (82,11,50,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (83,5,187,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (84,2,50,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (85,3,50,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (86,4,50,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (87,8,137,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (88,9,137,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (89,10,137,0,'11/27/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (90,6,241,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (91,7,59,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (92,11,18,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (93,5,241,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (94,2,59,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (95,3,59,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (96,4,59,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (97,8,182,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (98,9,182,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (99,10,182,0,'11/28/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (100,6,263,0,'11/29/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (101,7,88,0,'11/29/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (102,5,263,0,'11/29/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (103,2,88,0,'11/29/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (104,3,88,0,'11/29/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (105,4,108,0,'11/29/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (106,8,175,0,'11/29/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (107,9,175,0,'11/29/2006');
INSERT INTO [dbo].[BannerTracking] (TrackingId, BannerId, ImpressionCount, ClickCount, CreateDate) VALUES (108,10,175,0,'11/29/2006');