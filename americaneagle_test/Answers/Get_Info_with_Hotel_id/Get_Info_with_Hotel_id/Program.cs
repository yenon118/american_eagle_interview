﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Get_Info_with_Hotel_id
{
    class Program
    {
        static void Main(string[] args)
        {
            // Instantiate Hotel Credentials
            idevdesign.HotelCredentials hotelCredentials = new idevdesign.HotelCredentials();
            
            // Set username and password
            hotelCredentials.username = "aeTraining";
            hotelCredentials.password = "ZZZ";
            
            // Instantiate Hotel
            idevdesign.Hotels hotels = new idevdesign.Hotels();

            // Assign hotel credentials to hotels 
            hotels.HotelCredentialsValue = hotelCredentials;

            // Set hotel ID
            int hotelId = 105304;

            try{
                // Get hotel based on hotel ID
                idevdesign.Hotel hotel = hotels.GetHotel(hotelId);

                // Print the hotel ID, hotel name, hotel airport code, and hotal address
                Console.WriteLine(hotel.HotelID + ":\t" + hotel.Name + "\t" + hotel.AirportCode + "\t" + hotel.Address1 + "\t" + hotel.Address2 + "\t" + hotel.Address3);
            } catch (Exception e) {
                Console.WriteLine("Error: \n{0}\n\n\n", e);
            }
            Console.ReadKey();
        }
    }
}
