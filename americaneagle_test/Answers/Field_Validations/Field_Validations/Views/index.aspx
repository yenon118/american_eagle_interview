﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Field_Validations.Views.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>    
    <link rel="stylesheet" href="../assets/css/all.min.css"/> <!-- font-awesome (include all.css and webfonts folder) -->
    <link rel="stylesheet" href="../assets/css/bootstrap-social.css"/>
    <link rel="stylesheet" href="../assets/css/lightgallery.min.css"/>
    <link rel="stylesheet" href="../assets/css/index.css"/>

    <title>Field Validations</title>
</head>
<body>
    <%--<form id="form1" runat="server">
        <div>

        </div>
    </form>--%>

    <div class="container">
        <div class="row content justify-content-center">
            <div class="col-12">
                <h1 class="text-center">Input</h1>
            </div>
            <div class="col-auto">
                <form id="xform" runat="server">
                    <div class="form-group">
                        <asp:DropDownList ID="stateDropDownList" runat="server" CssClass="form-control mx-1 my-1">
                            <asp:ListItem Text="" Value="0"></asp:ListItem>
                            <asp:ListItem Value="1">Illinois</asp:ListItem>
                            <asp:ListItem Value="2">Indianna</asp:ListItem>
                            <asp:ListItem Value="3">Iowa</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="xTextBox" runat="server" CssClass="form-control mx-1 my-1" type="text"></asp:TextBox>
                        <asp:Button ID="submitButton" runat="server" Text="Submit" CssClass="btn btn-primary mx-1 my-1" ValidationGroup="AllValidators"/>
                    </div>
                    <div class="has-error">
                        <span class="help-block">
                            <asp:CustomValidator ID="CustomValidator" runat="server" OnServerValidate="serverSideValidation" ClientValidationFunction="clientSideValidation"
                            ErrorMessage = "At least one field is required!!!" Display="Dynamic" ValidationGroup="AllValidators"></asp:CustomValidator>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/lightgallery.min.js"></script>
    <script src="../assets/js/index.js"></script>

    <!-- A jQuery plugin that adds cross-browser mouse wheel support. (Optional) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

    <!-- lightgallery plugins -->
    <script src="../assets/js/lg-thumbnail.min.js"></script>
    <script src="../assets/js/lg-fullscreen.min.js"></script>

</body>
</html>

<script>
    function clientSideValidation(source, arguments) {
        var dropDownValue = document.getElementById('<%= stateDropDownList.ClientID %>');
        var textValue = document.getElementById('<%= xTextBox.ClientID %>');

        if (dropDownValue.options[dropDownValue.selectedIndex].value == 0 && textValue.value.length == 0) {
            arguments.IsValid = false;
        } else {
            arguments.IsValid = true;
        }
    }

</script>