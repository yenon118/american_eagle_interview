﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Field_Validations.Views
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void serverSideValidation(object source, ServerValidateEventArgs args)
        {
            if (xTextBox.Text.Length == 0 && stateDropDownList.SelectedItem.Text.Equals(""))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
    }
}