
var websiteLogo = document.getElementById("websiteLogo");
var websiteMenu = document.getElementById('websiteMenu');
var websiteMenuList = document.getElementById("websiteMenuList");
var aboutLink = document.getElementById("aboutLink");
var educationLink = document.getElementById("educationLink");
var experienceLink = document.getElementById("experienceLink");
var projectsLink = document.getElementById("projectsLink");
var skillsLink = document.getElementById("skillsLink");
var contactLink = document.getElementById("contactLink");

var rfidHSSLink = document.getElementById('rfidHSSLink');
var rfidHSSModal = document.getElementById('rfidHSSModal');
var rfidHSSGalaries = ["images/RfidHomeSecuritySystem/rfidHomeSecuritySystem1.PNG", "images/RfidHomeSecuritySystem/rfidHomeSecuritySystem2.PNG",
                      "images/RfidHomeSecuritySystem/rfidHomeSecuritySystem3.PNG", "images/RfidHomeSecuritySystem/rfidHomeSecuritySystem4.PNG",
                      "images/RfidHomeSecuritySystem/rfidHomeSecuritySystem5.PNG", "images/RfidHomeSecuritySystem/rfidHomeSecuritySystem6.PNG",
                      "images/RfidHomeSecuritySystem/rfidHomeSecuritySystem7.PNG", "images/RfidHomeSecuritySystem/rfidHomeSecuritySystem8.PNG"];
var rfidHSSImageNumber = 0;
var rfidHSSImage = document.getElementById('rfidHSSImage');
var rfidHSSPreviousLink = document.getElementById('rfidHSSPreviousLink');
var rfidHSSNextLink = document.getElementById('rfidHSSNextLink');
var rfidHSSSourceCode = document.getElementById('rfidHSSSourceCode');
var rfidHSSCloseSpan = document.getElementsByClassName("close")[0];

var rideXpressLink = document.getElementById('rideXpressLink');
var rideXpressModal = document.getElementById('rideXpressModal');
var rideXpressGalaries = ["images/RideXpress/Index.PNG", "images/RideXpress/Registration.PNG",
                        "images/RideXpress/Login.PNG", "images/RideXpress/Main.PNG",
                        "images/RideXpress/RidesList.PNG", "images/RideXpress/CurrentEmployeesList.PNG",
                        "images/RideXpress/AddEmployee.PNG", "images/RideXpress/TerminateEmployee.PNG",
                        "images/RideXpress/TasksList.PNG"];
var rideXpressImageNumber = 0;
var rideXpressImage = document.getElementById('rideXpressImage');
var rideXpressPreviousLink = document.getElementById('rideXpressPreviousLink');
var rideXpressNextLink = document.getElementById('rideXpressNextLink');
var rideXpressSourceCode = document.getElementById('rideXpressSourceCode');
var rideXpressRevature = document.getElementById('rideXpressRevature');
var rideXpressCloseSpan = document.getElementsByClassName("close")[1];

switchLogo();
switchWebsiteMenuList();

function switchLogo(){
  if ($(window).width() < 400) {
    websiteLogo.innerHTML = "Yen C.";
  }
  else{
    websiteLogo.innerHTML = "Yen On Chan";
  }
}

function switchWebsiteMenuList(){
  if ($(window).width() < 1100) {
    websiteMenuList.style.display = "none";
    websiteMenuList.style.float = "none";
  }
  else {
    websiteMenuList.style.display = "inline-block";
    websiteMenuList.style.float = "right";
  }
}

websiteMenu.onclick = function(){
  if(websiteMenuList.style.display === "none"){
    websiteMenuList.style.display = "block";
  }
  else{
    websiteMenuList.style.display = "none";
  }
}

$("#resumeButton").click(function() {
  window.open('documents/Yen On Chan - Resume.pdf','_blank');
});

$("#rfidHSSImage").click(function() {
  window.open(rfidHSSGalaries[Math.abs(rfidHSSImageNumber%8)],'_blank');
});

$("#rideXpressImage").click(function() {
  window.open(rideXpressGalaries[Math.abs(rideXpressImageNumber%9)],'_blank');
});

rfidHSSLink.onclick = function(){
  rfidHSSModal.style.display = "block";
  rfidHSSImage.src = rfidHSSGalaries[0];
}

rfidHSSCloseSpan.onclick = function() {
  rfidHSSModal.style.display = "none";
}

rfidHSSPreviousLink.onclick = function(){
  rfidHSSImageNumber--;
  rfidHSSImage.src = rfidHSSGalaries[Math.abs(rfidHSSImageNumber%8)];
}

rfidHSSNextLink.onclick = function(){
  rfidHSSImageNumber++;
  rfidHSSImage.src = rfidHSSGalaries[Math.abs(rfidHSSImageNumber%8)];
}

rfidHSSSourceCode.onclick = function(){
  window.open('https://github.com/yenon118/RFID_Home_Security_System','_blank');
}

rideXpressLink.onclick = function(){
  rideXpressModal.style.display = "block";
  rideXpressImage.src = rideXpressGalaries[0];
}

rideXpressCloseSpan.onclick = function() {
  rideXpressModal.style.display = "none";
}

rideXpressPreviousLink.onclick = function(){
  rideXpressImageNumber--;
  rideXpressImage.src = rideXpressGalaries[Math.abs(rideXpressImageNumber%9)];
}

rideXpressNextLink.onclick = function(){
  rideXpressImageNumber++;
  rideXpressImage.src = rideXpressGalaries[Math.abs(rideXpressImageNumber%9)];
}

rideXpressSourceCode.onclick = function(){
  window.open('https://github.com/yenon118/RideXpress','_blank');
}

rideXpressRevature.onclick = function(){
  window.open('https://revature.com/mizzou/','_blank');
}

window.onclick = function(event) {
  if (event.target == rfidHSSModal) {
    rfidHSSModal.style.display = "none";
  }

  if (event.target == rideXpressModal) {
    rideXpressModal.style.display = "none";
  }

  if(event.target == aboutLink || event.target == educationLink ||
  event.target == experienceLink || event.target == projectsLink ||
  event.target == skillsLink || event.target == contactLink){
    if(websiteMenuList.style.display === "block"){
      websiteMenuList.style.display = "none";
    }
  }
}

$('a[href^="#no_further_movement"]').click(function(e) {
    e.preventDefault();
});

function changeBodyOnResize(){
  switchLogo();
  switchWebsiteMenuList();
}
