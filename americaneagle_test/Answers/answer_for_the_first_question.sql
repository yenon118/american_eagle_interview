USE [webapp];

--SELECT * FROM [dbo].[Banner];
--SELECT * FROM [dbo].[BannerTracking];

--SELECT b.BannerId, b.Name, b.Link FROM [dbo].[Banner] AS b;

--SELECT t.BannerId, SUM(t.ImpressionCount) AS SumImpressionCount, SUM(t.ClickCount) AS SumClickCount 
--FROM [dbo].[BannerTracking] AS t GROUP BY t.BannerId;


/***** The answer for the (1) question *****/
SELECT b.Name, b.Link, SUM(t.ImpressionCount) AS SumImpressionCount, SUM(t.ClickCount) AS SumClickCount 
FROM [dbo].[Banner] b JOIN [dbo].[BannerTracking] AS t ON b.BannerId=t.BannerId 
WHERE t.CreateDate >= Convert(datetime, '2006-11-21' ) 
GROUP BY b.Name, b.Link;
