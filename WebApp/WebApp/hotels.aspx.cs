﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//namespace WebApp
//{

public partial class hotels : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Instantiate Hotel Credentials
            WebApp.GoHotel.HotelCredentials hotelCredentials = new WebApp.GoHotel.HotelCredentials();

            // Set username and password
            hotelCredentials.username = "aeTraining";
            hotelCredentials.password = "ZZZ";

            // Instantiate Hotel
            WebApp.GoHotel.Hotels hotels = new WebApp.GoHotel.Hotels();

            // Assign hotel credentials to hotels 
            hotels.HotelCredentialsValue = hotelCredentials;

            // Set hotel ID
            int hotelId = 105304;

            try
            {
                // Get hotel based on hotel ID
                WebApp.GoHotel.Hotel hotel = hotels.GetHotel(hotelId);
                hotel.Address1 = hotel.Address1 + hotel.Address2 + hotel.Address3;

                List <WebApp.GoHotel.Hotel> hotelList = new List<WebApp.GoHotel.Hotel>();
                hotelList.Add(hotel);

                // Print the hotel ID, hotel name, hotel airport code, and hotal address
                Console.WriteLine(hotel.HotelID + ":\t" + hotel.Name + "\t" + hotel.AirportCode + "\t" + hotel.Address1 + "\t" + hotel.Address2 + "\t" + hotel.Address3);

                GridView6.DataSource = hotelList;
                GridView6.DataBind();
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: \n{0}\n\n\n", exception);
            }
        }
        
    }
}

//}