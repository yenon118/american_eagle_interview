﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Validator.aspx.cs" Inherits="Validator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Validator</title>
</head>
<body>
    <%--<form id="form1" runat="server">
        <div>
        </div>
    </form>--%>

    <p style="color:red;"><b>Please put your work on these placeholder pages and use code-behind class files so we can see your code (please don't compile into DLL files):</b><br /></p>


    <a href="/">&laquo; Back To Home</a>

    <p>
        Please use the built-in .NET &lt;asp:CustomValidator&gt; control to validate the following:<br />
    </p>

    <ul>
        <li>If both fields are filled out, then an error should NOT appear.</li>
        <li>If one field is filled out, then an error should NOT appear.</li>
        <li>If neither field is filled out, then an error should appear.</li>
    </ul>

    <p>Please perform validation on the server and please make sure that Javascript (client-side) validation is performed as well. We've had several applicants forget to do the Javascript validation on the client-side so please make sure that you're doing both server-side and client-side validation.</p>

    <p>Server-side validation needs to be implemented on the code behind page.</p>

    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="drpState"  runat="server" CausesValidation="True">
                <asp:ListItem Text="" Value="0"></asp:ListItem>
                <asp:ListItem Value="IL">Illinois</asp:ListItem>
                <asp:ListItem Value="IN">Indiana</asp:ListItem>
                <asp:ListItem Value="IA">Iowa</asp:ListItem>
            </asp:DropDownList>

            <asp:TextBox ID="txtRegion" runat="server"></asp:TextBox>

            <asp:Button ID="btnSubmit" Text="Submit" runat="server" ValidationGroup="AllValidators"/>
        </div>
        <div>
            <asp:CustomValidator ID="CustomValidator" runat="server" OnServerValidate="serverSideValidation" ClientValidationFunction="clientSideValidation"
            ErrorMessage = "At least one field is required!!!" Display="Dynamic" ValidationGroup="AllValidators"></asp:CustomValidator>
        </div>
    </form>

</body>
</html>

<script>
    function clientSideValidation(source, arguments) {
        var dropDownValue = document.getElementById('<%= drpState.ClientID %>');
        var textValue = document.getElementById('<%= txtRegion.ClientID %>');

        if (dropDownValue.options[dropDownValue.selectedIndex].value == 0 && textValue.value.length == 0) {
            arguments.IsValid = false;
        } else {
            arguments.IsValid = true;
        }
    }

</script>