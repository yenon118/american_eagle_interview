﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//namespace WebApp
//{
public class BannerBLL
{
    private BannerDAL bannerDataAccessLayer;

    public BannerBLL(string connectionString)
    {
        bannerDataAccessLayer = new BannerDAL(connectionString);
    }

    public List<BannerBannerViewModel> getBannerColumnDetailsList()
    {
        List<BannerBannerViewModel> bannerColumnsInfoList = new List<BannerBannerViewModel>();
        foreach (BannerBannerViewModel detail in bannerDataAccessLayer.getBannerColumnDetails())
        {
            bannerColumnsInfoList.Add(detail);
        }
        return bannerColumnsInfoList;
    }

    public List<BannerBannerViewModel> getBannerTrackingColumnDetailsList()
    {
        List<BannerBannerViewModel> bannerTrackingColumnsInfoList = new List<BannerBannerViewModel>();
        foreach (BannerBannerViewModel detail in bannerDataAccessLayer.getBannerTrackingColumnDetails())
        {
            bannerTrackingColumnsInfoList.Add(detail);
        }
        return bannerTrackingColumnsInfoList;
    }

    public List<BannerDataViewModel> getAllBannerResultsList()
    {
        List<BannerDataViewModel> allBannerResultsList = new List<BannerDataViewModel>();
        foreach (BannerDataViewModel detail in bannerDataAccessLayer.getAllBannerResults())
        {
            allBannerResultsList.Add(detail);
        }
        return allBannerResultsList;
    }

    public List<BannerDataViewModel> getAllBannerTrackingResultsList()
    {
        List<BannerDataViewModel> allBannerTrackingResultsList = new List<BannerDataViewModel>();
        foreach (BannerDataViewModel detail in bannerDataAccessLayer.getAllBannerTrackingResults())
        {
            allBannerTrackingResultsList.Add(detail);
        }
        return allBannerTrackingResultsList;
    }

    public List<BannerDataViewModel> getCustomizedResultsList()
    {
        List<BannerDataViewModel> customizedResultsList = new List<BannerDataViewModel>();
        foreach (BannerDataViewModel detail in bannerDataAccessLayer.getCustomizedResults())
        {
            customizedResultsList.Add(detail);
        }
        return customizedResultsList;
    }

}
//}