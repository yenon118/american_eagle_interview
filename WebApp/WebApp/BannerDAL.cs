﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

//namespace WebApp
//{
public class BannerDAL
{
    private string connectionString;

    public BannerDAL(string connectionString)
    {
        this.connectionString = connectionString;
    }

    public List<BannerBannerViewModel> getBannerColumnDetails()
    {
        string sqlQuery = "exec sp_columns Banner;";
        List<BannerBannerViewModel> bannerColumnsInfo = new List<BannerBannerViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerBannerViewModel temp = new BannerBannerViewModel()
                    {
                        columnName = reader["COLUMN_NAME"].ToString(),
                        typeName = reader["TYPE_NAME"].ToString(),
                        length = Convert.ToInt32(reader["LENGTH"]),
                        nullable = Convert.ToInt32(reader["NULLABLE"]),
                        ordinalPosition = Convert.ToInt32(reader["ORDINAL_POSITION"])
                    };
                    bannerColumnsInfo.Add(temp);
                }
            }
        }
        return bannerColumnsInfo;
    }

    public List<BannerBannerViewModel> getBannerTrackingColumnDetails()
    {
        string sqlQuery = "exec sp_columns BannerTracking;";
        List<BannerBannerViewModel> bannerTrackingColumnsInfo = new List<BannerBannerViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerBannerViewModel temp = new BannerBannerViewModel()
                    {
                        columnName = reader["COLUMN_NAME"].ToString(),
                        typeName = reader["TYPE_NAME"].ToString(),
                        length = Convert.ToInt32(reader["LENGTH"]),
                        nullable = Convert.ToInt32(reader["NULLABLE"]),
                        ordinalPosition = Convert.ToInt32(reader["ORDINAL_POSITION"])
                    };
                    bannerTrackingColumnsInfo.Add(temp);
                }
            }
        }
        return bannerTrackingColumnsInfo;
    }

    public List<BannerDataViewModel> getAllBannerResults()
    {
        string sqlQuery = "SELECT * FROM Banner;";
        List<BannerDataViewModel> allBannerResults = new List<BannerDataViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerDataViewModel temp = new BannerDataViewModel()
                    {
                        bannerId = Convert.ToInt32(reader["BannerId"]),
                        name = reader["Name"].ToString(),
                        link = reader["Link"].ToString(),
                        image = reader["Image"].ToString(),
                        altText = reader.GetString(4).ToString(),
                        targetString = (reader["Target"]).ToString(),
                        isActive = Convert.ToInt32(reader["IsActive"])
                    };
                    allBannerResults.Add(temp);
                }
            }
        }
        return allBannerResults;
    }

    public List<BannerDataViewModel> getAllBannerTrackingResults()
    {
        string sqlQuery = "SELECT * FROM BannerTracking;";
        List<BannerDataViewModel> allBannerTrackingResults = new List<BannerDataViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerDataViewModel temp = new BannerDataViewModel()
                    {
                        trackingId = Convert.ToInt32(reader["TrackingId"]),
                        bannerId = Convert.ToInt32(reader["BannerId"]),
                        impressionCount = Convert.ToInt32(reader["ImpressionCount"]),
                        clickCount = Convert.ToInt32(reader["ClickCount"]),
                        createDate = reader["CreateDate"].ToString()
                    };
                    allBannerTrackingResults.Add(temp);
                }
            }
        }
        return allBannerTrackingResults;
    }

    public List<BannerDataViewModel> getCustomizedResults()
    {
        string sqlQuery = "SELECT b.Name, b.Link, SUM(t.ImpressionCount) AS SumImpressionCount, SUM(t.ClickCount) AS SumClickCount " + 
                            "FROM Banner b JOIN BannerTracking AS t ON b.BannerId = t.BannerId " + 
                            "WHERE t.CreateDate >= Convert(datetime, '2006-11-21') " + 
                            "GROUP BY b.Name, b.Link; ";
        List<BannerDataViewModel> customizedResults = new List<BannerDataViewModel>();
        using (SqlConnection con = new SqlConnection(this.connectionString))
        using (SqlCommand cmd = new SqlCommand(sqlQuery, con))
        {
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    BannerDataViewModel temp = new BannerDataViewModel()
                    {
                        name = reader["Name"].ToString(),
                        link = reader["Link"].ToString(),
                        impressionCount = Convert.ToInt32(reader["SumImpressionCount"]),
                        clickCount = Convert.ToInt32(reader["SumClickCount"])
                    };
                    customizedResults.Add(temp);
                }
            }
        }
        return customizedResults;
    }



}
//}