﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//namespace WebApp
//{

public class BannerDataViewModel
{
    public int bannerId { get; set; }

    public string name { get; set; }
    public string link { get; set; }
    public string image { get; set; }
    public string altText { get; set; }
    private string target;
    public string targetString
    {
        get
        {
            return target;
        }
        set
        {
            if (value.Equals("") || value == null)
            {
                target = "NULL";
            }
        }
    }
    public int isActive { get; set; }

    public int trackingId { get; set; }
    public int impressionCount { get; set; }
    public int clickCount { get; set; }
    public string createDate { get; set; }

}

//}