﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//namespace WebApp
//{
public partial class Banner : System.Web.UI.Page
{
    /*** Use either one (depends on which machine you run this code) ***/
    /*** You might need to edit the config file too. ***/

    //private string connectionString = ConfigurationManager.AppSettings["ConnectionString"].ToString();
    private string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindData();
        }
    }

    private void bindData()
    {
        BannerBLL bannerBLL = new BannerBLL(connectionString);

        GridView1.DataSource = bannerBLL.getBannerColumnDetailsList();
        GridView1.DataBind();

        GridView2.DataSource = bannerBLL.getBannerTrackingColumnDetailsList();
        GridView2.DataBind();

        GridView3.DataSource = bannerBLL.getAllBannerResultsList();
        GridView3.DataBind();

        GridView4.DataSource = bannerBLL.getAllBannerTrackingResultsList();
        GridView4.DataBind();

        GridView5.DataSource = bannerBLL.getCustomizedResultsList();
        GridView5.DataBind();
    }
}
//}